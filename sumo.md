# BITB Sumo

## Rules

* The event takes place inside a standard Beetleweight arena, on a raised plywood hexagonal surface 1.2 x 1.2m, from now referred to as the Hex
* There are two competitors
* Each competitor starts in their assigned side of the Hex (either 1 or 2)
* Each robot must start touching the back edge of their side of the Hex (meaning the opponents start as far apart as possible)
* The aim is to get your opponent out of the Hex
* The round ends as soon as at least 1 robot leaves the Hex
* Getting the opponent out of the Hex, crossing the border, on their starting side gives the winning robot 1 point
* Getting the opponent out of the Hex, crossing the border, on your starting side gives the winning robot 3 points
* The robot that leaves the Hex first gains no points
* There is a 2 minute limit, if neither competitor has left the Hex, at the end of the time limit, then neither competitor scores any points
* A robot is considered to have left the Hex if it touches the floor of the arena, outside of the Hex
* The ref will end the match if they believe a robot has touched the floor outside of the Hex. The refs decision is final
* In the event that both robots leave the Hex simultaneously, as decided by the ref, they will both receive 1 point (regardless of which Hex side they left)
* In the event that there is any doubt about which side of the Hex the robot left via, the judges will make the decision
* Weapons are allowed to be used
* Robots that have left the Hex must stop moving and turn off any weapons
* Normal arm up/safety rules apply
* Floor/Seam issues are considered part of the game

## The Hex

![the-hex.png](images/the-hex.png)

## Example rounds

### Example round 1

* `Saw-Loser` starts in side 1 of the Hex
* `Attitude Adjuster` starts in side 2 of the Hex
* `Saw-Loser` pushes `Attitude Adjuster` out of the Hex, `Attitude Adjuster` crosses an edge of side 2
* `Saw-Loser` gains 1 point

### Example round 2

* `Saw-Loser` starts in side 1 of the Hex
* `Attitude Adjuster` starts in side 2 of the Hex
* The two meet in the middle and `Attitude Adjuster` gets the pair turned around
* `Attitude Adjuster` gets `Saw-Loser` out of the Hex, `Saw-Loser` crosses an edge of side 2
* `Attitude Adjuster` gains 3 points

### Example round 3

* `End Boss` starts in side 1 of the Hex
* `Saw Loser` starts in side 2 of the Hex
* `End Boss` spins up its weapon, and makes a hit on `Saw Loser`
* Both robots fly apart from each other, from the impact, and both leave the Hex
* Both robots gain 1 point