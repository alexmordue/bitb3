from enum import Enum
import csv
from typing import List, Type
from jinja2 import Environment, PackageLoader

class WeaponType(Enum):
    AxeBot = 0
    DrillBot = 1
    DrumSpinner = 2
    EggBeater = 3
    Grabber = 5
    Hammer = 6
    HorizontalSpinner = 7
    Lifter = 8
    MultiWeaponBot = 9
    OverheadAttack = 10
    RamBot = 12
    SawBot = 13
    SitAndSpin = 14
    VerticalSpinner = 16
    Flipper = 17
    ClusterBot = 18
    Crusher = 19
    Unknown = 32


    def __str__(self):
        return '{0}'.format(self.name)
    
    @classmethod
    def from_str(cls, weapon_str: str):
        match weapon_str:
            case "hammersaw":
                return cls(cls.OverheadAttack)
            case "axe":
                return cls(cls.AxeBot)
            case "crusher":
                return cls(cls.Crusher)
            case "overheadspinner":
                return cls(cls.OverheadAttack)
            case "clusterbot":
                return cls(cls.ClusterBot)
            case "wagglesticks":
                return cls(cls.Grabber)
            case "fullbodyspinner":
                return cls(cls.HorizontalSpinner)
            case "wedge":
                return cls(cls.RamBot)
            case "flipper":
                return cls(cls.Flipper)
            case "axebot":
                return cls(cls.AxeBot)
            case "drillbot":
                return cls(cls.DrillBot)
            case "drumspinner":
                return cls(cls.DrumSpinner)
            case "eggbeater":
                return cls(cls.EggBeater)
            case "grabandlift":
                return cls(cls.Grabber)
            case "grabber":
                return cls(cls.Grabber)
            case "hammer":
                return cls(cls.Hammer)
            case "horizontalspinner":
                return cls(cls.HorizontalSpinner)
            case "lifter":
                return cls(cls.Lifter)
            case "multiweapon":
                return cls(cls.MultiWeaponBot)
            case "overheadsaw":
                return cls(cls.OverheadAttack)
            case "rambot":
                return cls(cls.RamBot)
            case "noweapon":
                return cls(cls.RamBot)
            case "sawbot":
                return cls(cls.SawBot)
            case "sitandspin":
                return cls(cls.SitAndSpin)
            case "undercutter":
                return cls(cls.HorizontalSpinner)
            case "verticalspinner":
                return cls(cls.VerticalSpinner)
            case "Axe Bot":
                return cls(cls.AxeBot)
            case "Drill Bot":
                return cls(cls.DrillBot)
            case "Drum Spinner":
                return cls(cls.DrumSpinner)
            case "Egg Beater":
                return cls(cls.EggBeater)
            case "Grab and Lift":
                return cls(cls.Grabber)
            case "Grabber":
                return cls(cls.Grabber)
            case "Hammer":
                return cls(cls.Hammer)
            case "Horizontal Spinner":
                return cls(cls.HorizontalSpinner)
            case "Lifter":
                return cls(cls.Lifter)
            case "Multi-weapon Bot":
                return cls(cls.MultiWeaponBot)
            case "Overhead Saw":
                return cls(cls.OverheadAttack)
            case "Ram Bot":
                return cls(cls.RamBot)
            case "Saw Bot":
                return cls(cls.SawBot)
            case "Sit And Spin":
                return cls(cls.SitAndSpin)
            case "Undercutter":
                return cls(cls.HorizontalSpinner)
            case "Vertical Spinner":
                return cls(cls.VerticalSpinner)
            case _:
                return cls(cls.Unknown)

class Robot:
    def __init__(self, roboteer: str, robot: str, weapon: str):
        self.roboteer = roboteer
        self.robot = robot
        self.weapon = WeaponType.from_str(weapon)
    def __str__(self) -> str:
        return ("{}, {}, {}".format(self.roboteer, self.robot, self.weapon))

class RobotList:
    def __init__(self, from_file: str):
        self.robots: List[Robot] = []
        
        with open(from_file) as csvfile:
            entrants_csv = csv.reader(csvfile)
            for entrant_line in entrants_csv:
                robot = Robot(entrant_line[0], entrant_line[1], entrant_line[2])
                self.robots.append(robot)
            self.robots.sort(key=lambda robot: robot.robot.lower())

    def getRobot(self, robot_name: str):
        for robot in self.robots:
            if robot_name == robot.robot:
                return robot
        return Robot("Unknown", "Unknown", "Unknown")

class Battle:
    def __init__(self, battle_name: str, event: str, winner: str , winner_weapon: str, loser: str, loser_weapon: str):
        self.battle_name = battle_name
        self.event = event
        self.winner = winner
        self.winner_weapon = WeaponType.from_str(winner_weapon)
        self.loser = loser
        self.loser_weapon = WeaponType.from_str(loser_weapon)
    def __str__(self):
        return("{}, {}, {}, {}, {}, {}".format(self.battle_name, self.event, self.winner, self.winner_weapon, self.loser, self.loser_weapon))

class BattleList:
    def __init__(self, from_file: str):
        self.battles: List[Battle] = []
        with open(from_file) as csvfile:
            fights_csv = csv.reader(csvfile)
            for fight_line in fights_csv:
                battle = Battle(fight_line[0], fight_line[1], fight_line[2], fight_line[3], fight_line[4], fight_line[5])
                self.battles.append(battle)

    def get_battles_for(self, robot: str):
        competed_in: List[Battle] = []
        for battle in self.battles:
            if robot == battle.loser or robot == battle.winner:
                competed_in.append(battle)
        return competed_in
    
    def get_winning_battles_for(self, robot: str):
        winning: List[Battle] = []
        battles = self.get_battles_for(robot)
        for battle in battles:
            if robot == battle.winner:
                winning.append(battle)
        winning.sort(key=lambda battle: battle.loser.lower())
        return winning

    def get_losing_battles_for(self, robot: str):
        losing: List[Battle] = []
        battles = self.get_battles_for(robot)
        for battle in battles:
            if robot == battle.loser:
                losing.append(battle)
        losing.sort(key=lambda battle: battle.winner.lower())
        return losing

    def winsAgainstWeaponType(self, robot: str):
        weapon_win_list = {}
        wins = self.get_winning_battles_for(robot)
        for win in wins:
            win_count = weapon_win_list.get(win.loser_weapon, 0)
            weapon_win_list[win.loser_weapon] = win_count+1
        return weapon_win_list
    
    def lossesAgainstWeaponType(self, robot: str):
        weapon_loss_list = {}
        losses = self.get_losing_battles_for(robot)
        for loss in losses:
            loss_count = weapon_loss_list.get(loss.winner_weapon, 0)
            weapon_loss_list[loss.winner_weapon] = loss_count+1
        return weapon_loss_list

def main():
    battle_list = BattleList("fight_list.csv")
    robot_list = RobotList("entrants.csv")
    

    battle_stats = {}

    for robot in robot_list.robots:
        robot_name = robot.robot
        battle_stat = {}

        robot_details = robot_list.getRobot(robot_name)

        battle_stat["robot_details"] = robot_details

        battles = battle_list.get_winning_battles_for(robot_name)
        battle_stat["wins"] = battles

        weapon_wins = battle_list.winsAgainstWeaponType(robot_name)
        battle_stat["weapon_wins"] = weapon_wins

        battles = battle_list.get_losing_battles_for(robot_name)
        battle_stat["losses"] = battles

        weapon_losses = battle_list.lossesAgainstWeaponType(robot_name)
        battle_stat["weapon_losses"] = weapon_losses

        battle_stats[robot_name]=battle_stat

    env = Environment(loader=PackageLoader("bitb-stats"))
    template = env.get_template("output.md.jinja")
    print(template.render(data=battle_stats))

if __name__ == "__main__":
    main()