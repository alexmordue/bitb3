# Robot stats for robots signed up to BITB3

Last years fights: [https://rampagebots.co.uk/member/events/tournament/55](https://rampagebots.co.uk/member/events/tournament/55)

[[_TOC_]]


# 7th Circle

VerticalSpinner by David Weston

Win/Loss record: `24` / `13`

## Wins

* `Baby Dead Bod` at `Robodojo Bw Round 3`
* `Baby Dead Bod` at `Robodojo Bw Round 5`
* `Boom Zoom` at `Techno Events: Battle In The Burgh 2`
* `Cardinal Sin` at `Techno Events: Battle In The Burgh 2`
* `Chucky` at `Robodojo Bw Round 4`
* `Chucky` at `War in the Wirral`
* `Do Ya?` at `Rapture Gaming Festival`
* `Downwards Spiral` at `Techno Events: Battle In The Burgh 2`
* `Dragon` at `Robodojo Bw Round 3`
* `Déjà Two: Multi-Grab Drifting` at `War in the Wirral`
* `EMP` at `Robot Rebellion - Rapture Gaming Festival`
* `Exhilarator` at `Rapture Gaming Festival`
* `Fang` at `War in the Wirral`
* `Firebolt` at `War in the Wirral`
* `little grey fergie` at `Robodojo Bw Round 4`
* `MONK YEET BANAN` at `Scouse Showdown 2`
* `Once bitten` at `War in the Wirral`
* `Outrun` at `Robodojo Bw Round 4`
* `Schnake` at `Robot Rebellion - Rapture Gaming Festival`
* `Screaming banshee` at `Robodojo Bw Round 5`
* `Sprocket Raccoon` at `Rapture Gaming Festival`
* `Step Up 3D` at `Robot Rebellion - Rapture Gaming Festival`
* `Stingbinder` at `Scouse Showdown`
* `Vertex` at `Scouse Showdown 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "HorizontalSpinner" : 6
    "Lifter" : 4
    "Flipper" : 5
    "OverheadAttack" : 1
    "Grabber" : 4
    "EggBeater" : 1
    "RamBot" : 2
    "VerticalSpinner" : 1
```



## Losses

* `Anomaly` at `Robot Rebellion - Rapture Gaming Festival`
* `Bakugo` at `Techno Events: Battle In The Burgh 2`
* `Gaelic gladiator` at `Robodojo Bw Round 5`
* `Ice Breaker` at `Robodojo Bw Round 4`
* `Ice Breaker` at `Robodojo Bw Round 5`
* `Metis` at `Rapture Gaming Festival`
* `NOT FRAGILE` at `BBB Summer Showdown Beetle Comp`
* `Oubley` at `Scouse Showdown`
* `Procrastination` at `BBB Summer Showdown Beetle Comp`
* `Swag Demon` at `Robot Rebellion - Rapture Gaming Festival`
* `Toxin` at `Rapture Gaming Festival`
* `Unstoppable Force.` at `War in the Wirral`
* `Voltaic` at `Scouse Showdown 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Lifter" : 1
    "DrumSpinner" : 2
    "EggBeater" : 3
    "VerticalSpinner" : 5
    "Flipper" : 1
    "RamBot" : 1
```



# Arcticfurno

VerticalSpinner by Mark Smith

Win/Loss record: `1` / `2`

## Wins

* `King Crossblow` at `Scouse Showdown 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Flipper" : 1
```



## Losses

* `Oubley` at `Scouse Showdown 2`
* `Vertex` at `Scouse Showdown 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "EggBeater" : 1
    "VerticalSpinner" : 1
```



# Baby Dead Bod

HorizontalSpinner by Thomas Weatherley

Win/Loss record: `21` / `18`

## Wins

* `Attitude Adjuster` at `Robot Rebellion - Rapture Gaming Festival`
* `Bby Shrekt` at `Techno Events: Battle In The Burgh 2`
* `Bby Shrektforce` at `Robodojo - Beetleweight League - Round 1`
* `Dolos` at `Techno Events: Battle In The Burgh 2`
* `Dolos` at `War in the Wirral`
* `Firebolt` at `Techno Events: Battle In The Burgh 2`
* `Fisher Slice` at `Techno Events: Battle In The Burgh 2`
* `Ice Breaker` at `War in the Wirral`
* `jelly mould` at `Robodojo Bw Round 3`
* `little grey fergie` at `Robodojo Bw Round 3`
* `little grey fergie` at `Robodojo Bw Round 4`
* `Metis 2.0 - Beetle Tendency` at `Robot Rebellion - Rapture Gaming Festival`
* `Mow Problems` at `Robodojo - Beetleweight League - Round 1`
* `Oubley` at `Techno Events: Battle In The Burgh 2`
* `Revice` at `Scouse Showdown 2`
* `Screaming banshee` at `Robodojo - Beetleweight League - Round 1`
* `Swag Demon` at `Robodojo - Beetleweight League - Round 1`
* `Use Your Wee-losion` at `Techno Events: Battle In The Burgh 2`
* `Voltaic` at `Scouse Showdown 2`
* `Waddles` at `Techno Events: Battle In The Burgh 2`
* `Yam` at `Robodojo - Beetleweight League - Round 1`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Hammer" : 1
    "VerticalSpinner" : 5
    "MultiWeaponBot" : 2
    "Flipper" : 2
    "AxeBot" : 1
    "RamBot" : 4
    "HorizontalSpinner" : 1
    "EggBeater" : 1
    "ClusterBot" : 1
    "DrumSpinner" : 2
    "Lifter" : 1
```



## Losses

* `7th Circle` at `Robodojo Bw Round 3`
* `7th Circle` at `Robodojo Bw Round 5`
* `Anomaly` at `Robot Rebellion - Rapture Gaming Festival`
* `Bby Shrekt` at `Robodojo Bw Round 3`
* `Chucky` at `Robodojo Bw Round 4`
* `Contradiction` at `Robot Rebellion - Rapture Gaming Festival`
* `Dolos` at `Robodojo - Beetleweight League - Round 1`
* `Dolos` at `Robodojo - Beetleweight League - Round 1`
* `Déjà Two: Multi-Grab Drifting` at `Robodojo Bw Round 5`
* `End Boss` at `Robodojo Bw Round 5`
* `Ice Breaker` at `Robodojo Bw Round 4`
* `Lilith` at `Robodojo Bw Round 4`
* `little grey fergie` at `Robodojo Bw Round 6`
* `Nightcall` at `Robodojo Bw Round 6`
* `Oubley` at `Robodojo Bw Round 5`
* `Swag Demon` at `Robodojo Bw Round 6`
* `The Rovengers` at `Robodojo Bw Round 6`
* `Yam` at `War in the Wirral`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 6
    "Lifter" : 1
    "Flipper" : 2
    "EggBeater" : 2
    "MultiWeaponBot" : 2
    "Grabber" : 1
    "RamBot" : 1
    "DrumSpinner" : 2
    "ClusterBot" : 1
```



# Bakugo

DrumSpinner by Chris Sowry

Win/Loss record: `8` / `3`

## Wins

* `7th Circle` at `Techno Events: Battle In The Burgh 2`
* `Cascade` at `War in the Wirral`
* `Déjà Two: Multi-Grab Drifting` at `Techno Events: Battle In The Burgh 2`
* `Fang` at `War in the Wirral`
* `Greg` at `War in the Wirral`
* `Messie Nessie` at `Techno Events: Battle In The Burgh 2`
* `Salvage One: Recon Protocol` at `Techno Events: Battle In The Burgh 2`
* `Zephyrus` at `BBB Beetle Champs 2022`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "VerticalSpinner" : 3
    "Lifter" : 2
    "Grabber" : 1
    "HorizontalSpinner" : 2
```



## Losses

* `Bish Bash Bosh` at `BBB Beetle Champs 2022`
* `Unstoppable Force.` at `War in the Wirral`
* `Uplift` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "ClusterBot" : 1
    "EggBeater" : 1
    "DrumSpinner" : 1
```



# Bby Shrekt

VerticalSpinner by Sam Price

Win/Loss record: `40` / `6`

## Wins

* `AOB` at `Robot Rebellion - Rapture Gaming Festival`
* `Babróg` at `Robot Rebellion - Rapture Gaming Festival`
* `Baby Dead Bod` at `Robodojo Bw Round 3`
* `Bourbon` at `Scouse Showdown`
* `Bourbon` at `Scouse Showdown`
* `BulletPoint Onryō` at `Techno Events: Battle In The Burgh 2`
* `Choppy 2` at `Scouse Showdown 2`
* `Chucky` at `Robodojo Bw Round 5`
* `Crossblow III` at `Robot Rebellion - Rapture Gaming Festival`
* `CSB: Echo` at `Robot Rebellion - Rapture Gaming Festival`
* `Dragon` at `Robodojo Bw Round 3`
* `Drizzle` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Gaelic gladiator` at `Robodojo Bw Round 3`
* `Gaelic gladiator` at `Robodojo Bw Round 6`
* `Ice Breaker` at `BBB Beetle Champs 2022`
* `Ice Breaker` at `Robodojo Bw Round 5`
* `IRVINE V1` at `Robodojo Bw Round 3`
* `King Crossblow` at `Scouse Showdown 2`
* `Kreigmesser` at `Techno Events: Battle In The Burgh 2`
* `Mantra` at `Scouse Showdown`
* `MotherLoader` at `BBB Beetle Champs 2022`
* `Night Fury` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Night Fury` at `Robot Rebellion - Rapture Gaming Festival`
* `Nightcall` at `Robodojo Bw Round 5`
* `Oh, Neil?` at `Scouse Showdown`
* `Oubley` at `Scouse Showdown`
* `Outrun` at `Robodojo Bw Round 3`
* `Procrastination` at `Scouse Showdown 2`
* `Rudimental` at `BBB Beetle Champs 2022`
* `Saw Loser: 2 Saw 2 Lose` at `Scouse Showdown`
* `Slav King` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Swag Demon` at `Robot Rebellion - Rapture Gaming Festival`
* `The Combobulator` at `Robodojo Bw Round 6`
* `The Combobulator` at `Robodojo Bw Round 6`
* `THE WIDENED ONE` at `Techno Events: Battle In The Burgh 2`
* `This is Not My Beautiful Wife EVO: Same As It Ever Was` at `Robot Rebellion - Rapture Gaming Festival`
* `TupperScare` at `Techno Events: Battle In The Burgh 2`
* `Uplift` at `Techno Events: Battle In The Burgh 2`
* `Vertex` at `Robodojo Bw Round 5`
* `Xerxes` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Flipper" : 4
    "Grabber" : 2
    "HorizontalSpinner" : 4
    "DrumSpinner" : 5
    "Lifter" : 4
    "RamBot" : 5
    "VerticalSpinner" : 10
    "EggBeater" : 5
    "OverheadAttack" : 1
```



## Losses

* `Baby Dead Bod` at `Techno Events: Battle In The Burgh 2`
* `End Boss` at `Scouse Showdown 2`
* `Fang` at `BBB Beetle Champs 2022`
* `Sproing!` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Stratus` at `Robot Rebellion - Rapture Gaming Festival`
* `Zephyrus` at `Robodojo Bw Round 6`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "HorizontalSpinner" : 2
    "VerticalSpinner" : 3
    "Flipper" : 1
```



# Beast Of Bodmin

Lifter by Tony Hastings

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Belladrum

DrumSpinner by John Rae

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Bonkers

VerticalSpinner by Stuart Brown

Win/Loss record: `2` / `1`

## Wins

* `Head For The Exit` at `BBB: Beetle Champs 2023`
* `New York Slice` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "SawBot" : 1
    "HorizontalSpinner" : 1
```



## Losses

* `Bad Daddy` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "EggBeater" : 1
```



# Boom Zoom

Lifter by Robert Weston

Win/Loss record: `15` / `8`

## Wins

* `Brug` at `Robot Rebellion - Rapture Gaming Festival`
* `Disaster Area` at `Robodojo Bw Round 5`
* `Disorder` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Gaelic gladiator` at `Robodojo Bw Round 6`
* `Keith the Teeth` at `BBB Beetle Champs 2022`
* `Kreigmesser` at `Robodojo Bw Round 6`
* `Let the good times stroll` at `Robot Rebellion - Rapture Gaming Festival`
* `little grey fergie` at `Robodojo Bw Round 5`
* `Nightcall` at `Robodojo Bw Round 5`
* `Nightcall` at `Robodojo Bw Round 6`
* `Panthor` at `Techno Events: Battle In The Burgh 2`
* `Rust in Pieces` at `Robot Rebellion - Rapture Gaming Festival`
* `Screaming banshee` at `Robodojo Bw Round 5`
* `This Is Not A Drill` at `Robot Rebellion - Rapture Gaming Festival`
* `Zephyrus` at `Robodojo Bw Round 6`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "RamBot" : 3
    "VerticalSpinner" : 6
    "Grabber" : 2
    "EggBeater" : 1
    "DrumSpinner" : 1
    "Lifter" : 2
```



## Losses

* `7th Circle` at `Techno Events: Battle In The Burgh 2`
* `End Boss` at `BBB Beetle Champs 2022`
* `Enigma` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Gertrude` at `BBB: Beetle Brawl 2022 - Beetleweight Robot Combat`
* `Ice Breaker` at `Robodojo Bw Round 6`
* `Night Fury` at `Robot Rebellion - Rapture Gaming Festival`
* `Uplift` at `Techno Events: Battle In The Burgh 2`
* `Uplift` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 3
    "Lifter" : 1
    "HorizontalSpinner" : 2
    "DrumSpinner" : 2
```



# Brassed Off

SawBot by Owen Fisher

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Brug Delüxe

SawBot by Dominic Johnstone

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# BulletPoint Onryō

Lifter by @iainiscreative

Win/Loss record: `0` / `3`

## Wins
No recorded wins


## Losses

* `Bby Shrekt` at `Techno Events: Battle In The Burgh 2`
* `Nezumi` at `Techno Events: Battle In The Burgh 2`
* `Speeny` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 1
    "Grabber" : 1
    "DrumSpinner" : 1
```



# BulletPoint Yurei

EggBeater by @iainiscreative

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# C-Mah

HorizontalSpinner by Dean

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Cormoran

SitAndSpin by Tony Hastings

Win/Loss record: `0` / `1`

## Wins
No recorded wins


## Losses

* `Ray` at `BBB: Beetle Brawl 2023 & Inter-Uni Competition`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Lifter" : 1
```



# Crofty Angel

Hammer by Tony Hastings

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Decalodon

Grabber by Dominic Cartlidge

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Dolos

MultiWeaponBot by Morgan

Win/Loss record: `6` / `3`

## Wins

* `Baby Dead Bod` at `Robodojo - Beetleweight League - Round 1`
* `Baby Dead Bod` at `Robodojo - Beetleweight League - Round 1`
* `Downwards Spiral` at `Techno Events: Battle In The Burgh 2`
* `Kreigmesser` at `Techno Events: Battle In The Burgh 2`
* `Procrastination` at `Techno Events: Battle In The Burgh 2`
* `Resistance` at `BBB Beetle Champs 2022`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "HorizontalSpinner" : 2
    "OverheadAttack" : 1
    "VerticalSpinner" : 2
    "RamBot" : 1
```



## Losses

* `Baby Dead Bod` at `Techno Events: Battle In The Burgh 2`
* `Baby Dead Bod` at `War in the Wirral`
* `Procrastination` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "HorizontalSpinner" : 2
    "VerticalSpinner" : 1
```



# Downwards Spiral

OverheadAttack by Jeremy Hall

Win/Loss record: `4` / `4`

## Wins

* `BSC` at `BBB Subterranean Showdown Beetleweight Competition`
* `Nitrite` at `BBB Subterranean Showdown Beetleweight Competition`
* `Speeny` at `Techno Events: Battle In The Burgh 2`
* `Zephyrus` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Crusher" : 1
    "VerticalSpinner" : 2
    "DrumSpinner" : 1
```



## Losses

* `7th Circle` at `Techno Events: Battle In The Burgh 2`
* `Dolos` at `Techno Events: Battle In The Burgh 2`
* `Kairos` at `BBB Subterranean Showdown Beetleweight Competition`
* `Swag Demon` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 1
    "MultiWeaponBot" : 1
    "SawBot" : 1
    "DrumSpinner" : 1
```



# Déjà Two: Multi-Grab Drifting

Grabber by Thomas Yau

Win/Loss record: `10` / `13`

## Wins

* `Baby Dead Bod` at `Robodojo Bw Round 5`
* `Fisher Slice` at `Techno Events: Battle In The Burgh 2`
* `Gaelic gladiator` at `Robot Rebellion - Rapture Gaming Festival`
* `Kilthane` at `War in the Wirral`
* `Leviathan` at `War in the Wirral`
* `little grey fergie` at `Robodojo Bw Round 6`
* `Revice` at `BBB Beetle Champs 2022`
* `Step Up 3D` at `BBB Beetle Champs 2022`
* `The Combobulator` at `Robodojo Bw Round 6`
* `Zephyrus` at `Robodojo Bw Round 6`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "HorizontalSpinner" : 2
    "AxeBot" : 1
    "EggBeater" : 1
    "VerticalSpinner" : 3
    "RamBot" : 1
    "ClusterBot" : 1
    "Lifter" : 1
```



## Losses

* `7th Circle` at `War in the Wirral`
* `Annie_R_U_OK (UK weight)` at `Robot Rebellion - Rapture Gaming Festival`
* `Bakugo` at `Techno Events: Battle In The Burgh 2`
* `Boring Bot` at `Robot Rebellion - Rapture Gaming Festival`
* `Delta V` at `War in the Wirral`
* `Disaster Area` at `Robodojo Bw Round 5`
* `Firebolt` at `Techno Events: Battle In The Burgh 2`
* `Gaelic gladiator` at `Robodojo Bw Round 5`
* `Ice Breaker` at `Robodojo Bw Round 6`
* `Lilith` at `BBB Beetle Champs 2022`
* `Oubley` at `Robodojo Bw Round 5`
* `Swag Demon` at `Robodojo Bw Round 6`
* `The Barber Surgeon` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 4
    "HorizontalSpinner" : 1
    "DrumSpinner" : 2
    "Grabber" : 1
    "Flipper" : 2
    "EggBeater" : 2
    "OverheadAttack" : 1
```



# Fisher Slice 2: No More Fisher Nice Guy

VerticalSpinner by Alys Rylance

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Fortunate Son

Lifter by Jack Ward-Downer

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Grab Crab

Grabber by Bristol Bot Builders

Win/Loss record: `4` / `3`

## Wins

* `Angel Slice` at `Robot Rebellion - Rapture Gaming Festival`
* `Dustwind` at `Robot Rebellion - Rapture Gaming Festival`
* `Paradigm Spin` at `Rapture Gaming Festival`
* `Vertex` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "HorizontalSpinner" : 2
    "RamBot" : 1
    "VerticalSpinner" : 1
```



## Losses

* `Rust in Pieces` at `Rapture Gaming Festival`
* `Slav King` at `Rapture Gaming Festival`
* `Stratus` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Lifter" : 1
    "DrumSpinner" : 1
    "VerticalSpinner" : 1
```



# Just a wee Slice

HorizontalSpinner by Euan Mutch

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Kenneth

AxeBot by Oliver Hatton

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Kilthane

VerticalSpinner by Thomas Weatherley

Win/Loss record: `0` / `1`

## Wins
No recorded wins


## Losses

* `Déjà Two: Multi-Grab Drifting` at `War in the Wirral`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Grabber" : 1
```



# Kreigmesser

VerticalSpinner by Mark Smith

Win/Loss record: `2` / `6`

## Wins

* `Sprocket Raccoon` at `Techno Events: Battle In The Burgh 2`
* `Uplift` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Lifter" : 1
    "DrumSpinner" : 1
```



## Losses

* `Bby Shrekt` at `Techno Events: Battle In The Burgh 2`
* `Boom Zoom` at `Robodojo Bw Round 6`
* `Chucky` at `Robodojo Bw Round 6`
* `Crazy horse` at `Robodojo Bw Round 6`
* `Dolos` at `Techno Events: Battle In The Burgh 2`
* `Ice Breaker` at `Robodojo Bw Round 6`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 2
    "Lifter" : 1
    "Flipper" : 1
    "RamBot" : 1
    "MultiWeaponBot" : 1
```



# Leviathan

HorizontalSpinner by Sion

Win/Loss record: `0` / `3`

## Wins
No recorded wins


## Losses

* `Déjà Two: Multi-Grab Drifting` at `War in the Wirral`
* `Saw Loser: 2 Saw 2 Lose` at `War in the Wirral`
* `Sniper` at `War in the Wirral`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Grabber" : 1
    "OverheadAttack" : 1
    "HorizontalSpinner" : 1
```



# Mecha-Panthor

VerticalSpinner by Reuben Stapley

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Messie Nessie

HorizontalSpinner by John Rae

Win/Loss record: `0` / `3`

## Wins
No recorded wins


## Losses

* `Bakugo` at `Techno Events: Battle In The Burgh 2`
* `Firebolt` at `Techno Events: Battle In The Burgh 2`
* `THE WIDENED ONE` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "DrumSpinner" : 1
    "Flipper" : 1
    "EggBeater" : 1
```



# New York Slice

HorizontalSpinner by Robert Webb

Win/Loss record: `0` / `2`

## Wins
No recorded wins


## Losses

* `Bonkers` at `BBB: Beetle Champs 2023`
* `Stratus` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 2
```



# Nezumi

Grabber by Nathan Burgess

Win/Loss record: `1` / `3`

## Wins

* `BulletPoint Onryō` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Lifter" : 1
```



## Losses

* `THE WIDENED ONE` at `Techno Events: Battle In The Burgh 2`
* `Uplift` at `Techno Events: Battle In The Burgh 2`
* `Waddles` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "EggBeater" : 1
    "DrumSpinner" : 1
    "Lifter" : 1
```



# Oubley

EggBeater by Sam graham

Win/Loss record: `6` / `3`

## Wins

* `7th Circle` at `Scouse Showdown`
* `Arcticfurno` at `Scouse Showdown 2`
* `Baby Dead Bod` at `Robodojo Bw Round 5`
* `Déjà Two: Multi-Grab Drifting` at `Robodojo Bw Round 5`
* `Procrastination` at `Scouse Showdown`
* `Resistance` at `BBB Summer Showdown Beetle Comp`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "VerticalSpinner" : 3
    "HorizontalSpinner" : 1
    "Grabber" : 1
    "RamBot" : 1
```



## Losses

* `Baby Dead Bod` at `Techno Events: Battle In The Burgh 2`
* `Bby Shrekt` at `Scouse Showdown`
* `Procrastination` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "HorizontalSpinner" : 1
    "VerticalSpinner" : 2
```



# Percussive Maintenance XL

AxeBot by Thomas Yau

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# PiHole

DrillBot by David Dolman

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Ray

Lifter by Pip Bruce

Win/Loss record: `2` / `3`

## Wins

* `Cormoran` at `BBB: Beetle Brawl 2023 & Inter-Uni Competition`
* `Reptile Dysfunction` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "SitAndSpin" : 1
    "VerticalSpinner" : 1
```



## Losses

* `Babróg` at `BBB: Beetle Brawl 2023 & Inter-Uni Competition`
* `Saw Loser: 2 Saw 2 Lose` at `BBB: Beetle Brawl 2023 & Inter-Uni Competition`
* `Wajoo` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Grabber" : 1
    "OverheadAttack" : 1
    "HorizontalSpinner" : 1
```



# Resistance

RamBot by Gareth Anstee

Win/Loss record: `11` / `6`

## Wins

* `BadFutherMucker` at `BBB Beetle Champs 2021`
* `Boring Bot` at `BBB Summer Showdown Beetle Comp`
* `Brug` at `BBB Summer Showdown Beetle Comp`
* `Carbonated Beverage` at `BBB Beetle Champs 2022`
* `Drizzle` at `BBB Beetle Champs 2021`
* `End Boss` at `BBB Beetle Champs 2021`
* `Fang` at `BBB Beetle Champs 2022`
* `Griefcase` at `BBB Summer Showdown Beetle Comp`
* `Igor` at `BBB Beetle Champs 2021`
* `Rust in Pieces` at `BBB Summer Showdown Beetle Comp`
* `Spoopy` at `BBB Beetle Champs 2021`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "VerticalSpinner" : 4
    "Grabber" : 1
    "RamBot" : 1
    "Flipper" : 2
    "HorizontalSpinner" : 1
    "Lifter" : 2
```



## Losses

* `Axeasaurus Rex` at `BBB: Beetle Champs 2023`
* `Dolos` at `BBB Beetle Champs 2022`
* `Inferno` at `BBB Beetle Champs 2021`
* `K1` at `BBB Summer Showdown Beetle Comp`
* `Oubley` at `BBB Summer Showdown Beetle Comp`
* `Yam` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "AxeBot" : 1
    "MultiWeaponBot" : 1
    "EggBeater" : 3
    "DrumSpinner" : 1
```



# Speeny

DrumSpinner by Lu Wylie

Win/Loss record: `4` / `4`

## Wins

* `BulletPoint Onryō` at `Techno Events: Battle In The Burgh 2`
* `Deadpan` at `Techno Events: Battle In The Burgh 2`
* `Loki` at `BBB: Beetle Brawl 2023 & Inter-Uni Competition`
* `Sprocket Raccoon` at `Techno Events: Battle In The Burgh 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Lifter" : 2
    "HorizontalSpinner" : 1
    "Grabber" : 1
```



## Losses

* `Downwards Spiral` at `Techno Events: Battle In The Burgh 2`
* `EVA 02` at `BBB: Beetle Champs 2023`
* `Rudimental` at `BBB: Beetle Brawl 2023 & Inter-Uni Competition`
* `Talon (Bite Size)` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "OverheadAttack" : 2
    "Lifter" : 1
    "Crusher" : 1
```



# The Barber Surgeon

OverheadAttack by Oliver Stapley

Win/Loss record: `1` / `6`

## Wins

* `Déjà Two: Multi-Grab Drifting` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "Grabber" : 1
```



## Losses

* `Annie_R_U_OK (UK weight)` at `Robot Rebellion - Rapture Gaming Festival`
* `Boring Bot` at `Robot Rebellion - Rapture Gaming Festival`
* `Gaelic gladiator` at `Robot Rebellion - Rapture Gaming Festival`
* `Grabber Wobba` at `BBB Subterranean Showdown Beetleweight Competition`
* `Ice Breaker` at `BBB Subterranean Showdown Beetleweight Competition`
* `Lööphöle` at `BBB Subterranean Showdown Beetleweight Competition`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "HorizontalSpinner" : 1
    "Grabber" : 2
    "EggBeater" : 1
    "VerticalSpinner" : 1
    "ClusterBot" : 1
```



# THE WIDENED ONE V2: 2 wide 4 u

EggBeater by Alys Rylance

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Voltageist

Lifter by Jimmi Cottam

Win/Loss record: `2` / `2`

## Wins

* `Digestive` at `Robot Rebellion - Rapture Gaming Festival`
* `Sir Lance-a-frog` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "VerticalSpinner" : 1
    "Lifter" : 1
```



## Losses

* `BadFutherMucker` at `Robot Rebellion - Rapture Gaming Festival`
* `Grapple Turnover` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "VerticalSpinner" : 1
    "Grabber" : 1
```



# Vortex BW

VerticalSpinner by Sean McGhee

Win/Loss record: `0` / `0`

## Wins
No recorded wins


## Losses
No recorded losses


# Yam

DrumSpinner by Morgan

Win/Loss record: `2` / `1`

## Wins

* `Baby Dead Bod` at `War in the Wirral`
* `Resistance` at `BBB: Beetle Champs 2023`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "HorizontalSpinner" : 1
    "RamBot" : 1
```



## Losses

* `Baby Dead Bod` at `Robodojo - Beetleweight League - Round 1`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "HorizontalSpinner" : 1
```



# Zephyrus

VerticalSpinner by Morgan

Win/Loss record: `8` / `12`

## Wins

* `Bby Shrekt` at `Robodojo Bw Round 6`
* `Bodhran` at `Techno Events: Battle In The Burgh 2`
* `Choppy Boi` at `Scouse Showdown`
* `little grey fergie` at `Robodojo Bw Round 6`
* `Rudimental` at `BBB Beetle Champs 2022`
* `Sproing!` at `Robot Rebellion - Rapture Gaming Festival`
* `TupperScare` at `Techno Events: Battle In The Burgh 2`
* `TupperScare` at `Scouse Showdown 2`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Wins against weapon type
    "VerticalSpinner" : 1
    "DrumSpinner" : 1
    "RamBot" : 4
    "Lifter" : 1
    "Flipper" : 1
```



## Losses

* `Aggro Wobba Smidders Edition` at `Robot Rebellion - Rapture Gaming Festival`
* `Babróg` at `Robot Rebellion - Rapture Gaming Festival`
* `Bakugo` at `BBB Beetle Champs 2022`
* `Boom Zoom` at `Robodojo Bw Round 6`
* `Downwards Spiral` at `Techno Events: Battle In The Burgh 2`
* `Déjà Two: Multi-Grab Drifting` at `Robodojo Bw Round 6`
* `End Boss` at `Scouse Showdown 2`
* `Ice Breaker` at `Scouse Showdown 2`
* `Mantra` at `Scouse Showdown`
* `Nightcall` at `Robodojo Bw Round 6`
* `Sir Lance-a-frog` at `BBB Beetle Champs 2022`
* `Ultra-Violence` at `Robot Rebellion - Rapture Gaming Festival`

```mermaid
%%{init: {'theme':'default'}}%%
pie showData title Losses to weapon type
    "Lifter" : 3
    "Grabber" : 2
    "DrumSpinner" : 1
    "OverheadAttack" : 1
    "VerticalSpinner" : 3
    "EggBeater" : 2
```



